import { ADDROUTES, SETREQUESTERROR } from "../Actions/types";

const initialState = { routes: null, error: false };

export const routeReducer = (state = initialState, actions) => {
  if (actions.type === ADDROUTES) {
    return { routes: actions.routes };
  }

  if (actions.type === SETREQUESTERROR) {
    return { ...state, error: actions.value };
  }

  return state;
};
