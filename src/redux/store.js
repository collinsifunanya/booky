import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import { routeReducer } from "./Reducers/routeReducer";
const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware));
const allReducers = combineReducers({ routeReducer });
// The store now has the ability to accept thunk functions in `dispatch`
const store = createStore(allReducers, composedEnhancer);
export default store;
