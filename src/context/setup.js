import { useQuery } from "react-query";
import { useDispatch } from "react-redux";
import { createContext, useContext, useEffect } from "react";
import Requests from "../utils/request";
import { ADDROUTES, SETREQUESTERROR } from "../redux/Actions/types";
import Loading from "../components/Loader";
import router from "next/router";
const AppContext = createContext();

function AppSetUpProvider({ children }) {
  const dispatch = useDispatch();
  const { data, isError, isLoading, error } = useQuery(
    "routes",
    () => Requests.fetchWithOutAuthToken("api/getRoutes", "GET"),
    { refetchOnWindowFocus: false }
  );

  const value = {
    routes: data,
    isError,
    isLoading,
    error,
  };

  useEffect(() => {
    if (isError) {
      return dispatch({ type: SETREQUESTERROR, value: true });
    }
    if (data) {
      return dispatch({ type: ADDROUTES, routes: data });
    }
  }, [data, isLoading]);

  if (isLoading) {
    return <Loading />;
  }

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
}

const useAppSetUp = () => {
  const context = useContext(AppContext);

  if (context === undefined) {
    throw new Error("useAppSetUp must be used within an AppSetUpProvider");
  }

  return context;
};

export { AppSetUpProvider, useAppSetUp };
