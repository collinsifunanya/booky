const Requests = {};

Requests.fetchWithOutAuthToken = async (url, method) => {
  const response = await fetch(url, {
    method: method, // *GET, POST, PUT, DELETE, etc.
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (response.ok) {
    return response.json();
  }

  throw { status: response.status, message: response.statusText };
};

export default Requests;
