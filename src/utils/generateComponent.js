import Container from "../components/Container";
import RichText from "../components/RichText";
import logo from "../images/placeholder.webp";

const generateComponent = (type, props) => {
  if (type === "container") {
    return <Container {...props} />;
  }

  if (type === "rich-text") {
    return <RichText {...props} />;
  }

  if (type === "image") {
    return (
      <img
        {...props}
        onError={(image) =>
          (image.target.src = "https://via.placeholder.com/100x100.png")
        }
      />
    );
  }
};

export default generateComponent;
