import React from "react";
import Loading from "./LoadingIcon";

function Loader(props) {
  return (
    <div className="h-screen flex items-center justify-center">
      <Loading />
    </div>
  );
}

export default Loader;
