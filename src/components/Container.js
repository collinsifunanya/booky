import React from "react";
import generateComponent from "../utils/generateComponent";

function Container({
  items,
  className,
  flexDirection,
  flexWrap,
  justifyContent,
  alignItems,
  width,
}) {
  return (
    <div
      style={{
        width: `${width * 100}%`,
        flexDirection,
        flexWrap,
        justifyContent,
        alignItems,
        display: "flex",
      }}
      className={className}
    >
      {items.map((item, index) => (
        <React.Fragment key={`${item.type}-${index}`}>
          {generateComponent(item.type, item)}
        </React.Fragment>
      ))}
    </div>
  );
}

export default Container;
