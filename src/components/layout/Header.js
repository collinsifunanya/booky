import Link from "next/link";
import { useSelector } from "react-redux";

function Header(props) {
  const { routes } = useSelector((state) => state.routeReducer);
  const transformedRoutes = Object.keys(routes || {});

  return (
    <nav className="flex justify-between lg:px-20 px-5 my-5 border-0 border-b">
      <div>
        <Link href="/">
          <span className="text-lg cursor-pointer">
            B<strong className="text-primary text-2xl">oo</strong>ky
          </span>
        </Link>
      </div>
      <div className="flex space-x-5">
        {transformedRoutes.map((route) => (
          <Link href={route || "/"} key={route}>
            <a>{routes[route]}</a>
          </Link>
        ))}
      </div>

      <div>
        <button>Register</button>
      </div>
    </nav>
  );
}

export default Header;
