import React from "react";
import parse from "html-react-parser";

function RichText({ textType, className, text, style }) {
  return (
    <>
      {textType === "html" ? (
        parse(text, {
          replace: (domNode) => {
            if (style === "uppercase") {
              return (
                <domNode.name style={{ textTransform: "uppercase" }}>
                  {domNode.children[0].data}
                </domNode.name>
              );
            }
          },
        })
      ) : (
        <p>{text}</p>
      )}
    </>
  );
}

export default RichText;
