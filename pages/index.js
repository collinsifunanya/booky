import { useEffect } from "react";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { useAppSetUp } from "../src/context/setup";
import Header from "../src/components/layout/Header";
import Link from "next/link";

export default function Home() {
  const router = useRouter();

  //I was asked to use redux to store the routes  (The reason why I'm have to data points to pull it)
  const { error, routes } = useSelector((state) => state.routeReducer);

  // I also have a setupContext, I can also pull the routes from my custom hook
  const { routes: routesfromContext, isLoading } = useAppSetUp();

  //for the sake of the project, I will utilize the data coming from the redux store throughout the project

  useEffect(() => {
    if (error) {
      router.replace("/maintenance");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error]);
  return (
    <div className="">
      <Header />

      <section className="h-screen lg:my-32">
        <h1 className="text-3xl font-extrabold text-center">
          Welcome to Booky
        </h1>
        <p className="text-center">Here is what you can do on Booky</p>

        <div className="lg:w-2/4 lg:h-60 m-auto  my-10 border p-5 rounded-lg flex items-center justify-between">
          {Object.keys(routes || {}).map((route, index) => (
            <Link href={route} key={`${route}-${index}`}>
              <div className="h-32 w-full border flex items-center justify-center cursor-pointer">
                <p>{routes[route]}</p>
              </div>
            </Link>
          ))}
        </div>
      </section>
    </div>
  );
}
