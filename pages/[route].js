import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import Header from "../src/components/layout/Header";
import generateComponent from "../src/utils/generateComponent";
import Loader from "../src/components/Loader";
import Requests from "../src/utils/request";

function DyamicPage({ contents, route }) {
  const router = useRouter();
  const { routes, error } = useSelector((state) => state.routeReducer);
  const arraysOfRoutes = Object.keys(routes || {});

  useEffect(() => {
    const routes = arraysOfRoutes[0] || "/";
    if (!arraysOfRoutes.includes(`/${route}`) && route) {
      router.push(routes);
    }
  }, [arraysOfRoutes]);

  if (router.isFallback) {
    return <Loader />;
  }

  if (error) {
    router.replace("/maintenance");
  }

  return (
    <div>
      <Header />
      <section className="lg:px-20 px-5 py-10">
        {contents?.map((content, index) => (
          <React.Fragment key={`${content.type}-${index}`}>
            {generateComponent(content.type, content)}
          </React.Fragment>
        ))}
      </section>
    </div>
  );
}
export async function getStaticPaths() {
  let routes;
  try {
    const response = await Requests.fetchWithOutAuthToken(
      "https://raw.githubusercontent.com/Bounteous-Inc/headless-cms-assessment/main/routes.json",
      "GET"
    );

    routes = Object.keys(response);
  } catch (error) {
    routes = [];
  }

  const paths = routes?.map((route) => ({
    params: {
      route: route,
    },
  }));

  return {
    fallback: true,
    paths: paths,
  };
}

export async function getStaticProps(context) {
  const route = context.params.route;
  let content;
  let errors;
  try {
    content = await Requests.fetchWithOutAuthToken(
      `https://raw.githubusercontent.com/Bounteous-Inc/headless-cms-assessment/main/${route}.json`,
      "GET"
    );
  } catch (err) {
    if (err.status !== 404) {
      errors = err;
    }
  }

  return {
    props: {
      contents: content ? content : [],
      route,
    },
  };
}

export default DyamicPage;
