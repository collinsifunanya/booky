import React from "react";
import MaintenanceIcon from "../src/components/svgs/Maintenance";

function maintenance(props) {
  return (
    <section className="h-screen flex items-center justify-center">
      <div>
        <MaintenanceIcon />

        <p>OOPS something went wrong</p>
      </div>
    </section>
  );
}

export default maintenance;
