import { QueryClient, QueryClientProvider } from "react-query";
import { Provider } from "react-redux";
import { AppSetUpProvider } from "../src/context/setup";
import store from "../src/redux/store";
import "../src/styles/globals.css";

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
        <AppSetUpProvider>
          <Component {...pageProps} />
        </AppSetUpProvider>
      </Provider>
    </QueryClientProvider>
  );
}

export default MyApp;
