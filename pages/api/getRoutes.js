// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import Requests from "../../src/utils/request";

export default function handler(req, res) {
  return Requests.fetchWithOutAuthToken(
    "https://raw.githubusercontent.com/Bounteous-Inc/headless-cms-assessment/main/routes.json"
  )
    .then((resp) => {
      return res.status(200).json(resp);
    })
    .catch((error) => {
      return res.status(error.status).send(error);
    });
}
