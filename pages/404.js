import React from "react";

function FourOFour(props) {
  return (
    <div>
      <h1>Redirecting.....</h1>
    </div>
  );
}

export default FourOFour;
